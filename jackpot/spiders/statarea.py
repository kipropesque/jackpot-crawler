# -*- coding: utf-8 -*-
#Sider for gathering info from various sites about the prediction of jackpot games 
import scrapy
from scrapy.http import Request
from jackpot.items import statAreaItems
from fuzzywuzzy import fuzz
import json
from datetime import datetime
from dateutil import tz

def loadSPFile():
	try:
		with open('sportpesa.json') as data_file:
			data = json.load(data_file)
	except IOError: # parent of IOError, OSError *and* WindowsError where available
		return []
	return data

def changeTimeZone(dt):
	from_zone = tz.gettz('Canada/Pacific')
	to_zone = tz.gettz('Africa/Nairobi')
	originaltime = datetime.strptime(dt, '%Y-%m-%d %H:%M')
	#Add tzone info to datetime
	originaltime = originaltime.replace(tzinfo=from_zone)
	#Convert back to Nairobi timezone
	nairobitime = originaltime.astimezone(to_zone)
	return nairobitime.strftime('%H:%M')


def matchdates(data):
	dates = []
	for d in data:
		if d['date'] not in dates:
			dates.append(d['date'])
	return dates

def formatDates(dates, currentformat, expectedformat):
	formattedDates = []
	for date in dates:
		print date
		dateObj = datetime.strptime(date, currentformat).date()
		cleandate = dateObj.strftime(expectedformat)
		formattedDates.append(cleandate)
	return formattedDates

#Search if team a == team b
def fuzzysearch(spteams, stmatches, timesp, timest):
	## Match times first 
	if(timesp.strip() == changeTimeZone(timest.strip())):
		#if fuzzywuzzy ration > 70 Good
		if(fuzz.ratio(spteams, stmatches) > 72 ):
			print "Found match between "+spteams+" and "+stmatches+ "\n"
			return True
	return False

#Add tips to sportpesa json file
# def addTips(spjson, item):
# 	tips = {}
# 	for event in spjson:
# 		if event['pos'] = item['pos']:
# 			tips['tips'] = str(item['tips'])
# 			event.update(tips)

class StatareaSpider(scrapy.Spider):
	name = "statarea"
	allowed_domains = ["http://www.statarea.com/"]
	dates = matchdates(loadSPFile())

	def start_requests(self):
		urldates = formatDates(self.dates, '%d/%m/%y', '%Y-%m-%d')
		for urldate in urldates:
			yield Request('http://www.statarea.com/predictions/date/'+urldate+'/competition', self.parse)

	def parse(self, response):
		data = loadSPFile()
		items = statAreaItems()
		urlparts = response.url.split('/')
		date = []
		date.append(urlparts[-2])
		datef = formatDates(date, '%Y-%m-%d', '%d/%m/%y')

		for event in data:
			if (event['date'] == datef[0]):
				matches = response.css('.prdmatch')
				for matchinfo in matches:
					hometeam = matchinfo.css('.prdhostteam a::text').extract()[0].lower().strip()
					awayteam = matchinfo.css('.prdguestteam a::text').extract()[0].lower().strip()
					timest = matchinfo.css('.matchtime ::text').extract()[0]
					if(fuzzysearch(event['homet'].strip().lower(), hometeam, event['time'], timest) or  fuzzysearch(event['awayt'].strip().lower(), awayteam, event['time'], timest )):
						items['pos'] = event['pos']
						items['tips'] = matchinfo.css('.tipfield span::text').extract()

						print event['homet'] +" - "+event['awayt'] + " odds --> "+''.join(items['tips'])
						yield(items)
