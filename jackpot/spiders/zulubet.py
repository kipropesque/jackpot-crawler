# -*- coding: utf-8 -*-
#Sider for gathering info from various sites about the prediction of jackpot games 
from datetime import datetime
from scrapy.http import Request
from jackpot.items import zuluBetItems
from fuzzywuzzy import fuzz
import scrapy
import json

def loadSPFile():
	try:
		with open('sportpesa.json') as data_file:
			data = json.load(data_file)
	except IOError:
		return []
	return data

def matchdates(data):
	dates = []
	for d in data:
		if d['date'] not in dates:
			dates.append(d['date'])
	return dates

def formatDates(dates, currentformat, expectedformat):
	formattedDates = []
	for date in dates:
		dateObj = datetime.strptime(date, currentformat).date()
		cleandate = dateObj.strftime(expectedformat)
		formattedDates.append(cleandate)
	return formattedDates


class zulubetSpider(scrapy.Spider):
	name = "zulubet"
	allowed_domains = ["http://www.zulubet.com/"]
	dates = matchdates(loadSPFile())

	def start_requests(self):
		urldates = formatDates(self.dates, '%d/%m/%y', '%d-%m-%Y')
		for urldate in urldates:
			yield Request('http://www.zulubet.com/tips-'+urldate+'.html', self.parse)

	def parse(self, response):
		data = loadSPFile()
		items = zuluBetItems()
		urlpart = response.url.split('/')[-1]
		date = []
		date.append(urlpart.replace('tips-','').replace('.html',''))
		datef = formatDates(date, '%d-%m-%Y', '%d/%m/%y')

		for event in data:
			if (event['date'] == datef[0]):
				teams = response.css('.content_table .flags').xpath('..')
				for team in teams:
					firstteam = team.css('::text').extract()[0].split('-')[0].strip()
					#To.DO fix issue with MK-DONS - Milton Keynes Dons
					if(fuzz.ratio(firstteam, event['homet'].lower().strip()) > 70 ):
						odds = team.xpath('..').xpath('td')[6].css('::text').extract()
						items['tips'] = list(odds)
						items['pos'] = event['pos']
						print event['homet'] +" - "+event['awayt'] + " odds --> "+''.join(items['tips'])
						yield(items)
