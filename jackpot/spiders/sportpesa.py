# -*- coding: utf-8 -*-
import scrapy

from jackpot.items import JackpotItem

class SportpesaSpider(scrapy.Spider):
	name = "sportpesa"
	allowed_domains = ["sportpesa.com"]
	start_urls = (
		'https://www.sportpesa.com/games/jackpot_games/',
	)
	custom_settings = {
		'ITEM_PIPELINES': {
			'jackpot.pipelines.JackpotPipeline': 300
		}
	}

	def parse(self, response):

		tablerows = response.css('table.mod-table tbody tr')

		for tr in tablerows:
			td = tr.xpath('td')
			item = JackpotItem()

			item['pos'] =  ''.join(td[0].xpath('text()').extract())
			item['time'] = ''.join(td[1].xpath('text()').extract())
			item['date'] = ''.join(td[2].css('span.text-left').xpath('text()').extract())
			item['flag'] = ''.join(td[2].css('span.text-right').xpath('img/@src').extract())
			item['homet'] = ''.join(td[3].css('span.text-left').xpath('text()').extract())
			item['hometodds'] = ''.join(td[3].css('span.text-right span.text-right').xpath('text()').extract())
			item['drawodds'] = ''.join(td[4].xpath('text()').extract())
			item['awayt'] = ''.join(td[5].css('span.text-left').xpath('text()').extract())
			item['awaytodds'] = ''.join(td[5].css('span.text-right').xpath('text()').extract())
			item['result'] = ''.join(td[6].xpath('text()').extract())
			yield(item)
