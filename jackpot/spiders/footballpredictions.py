# -*- coding: utf-8 -*-
#Sider for gathering info from various sites about the prediction of jackpot games 
import scrapy
from scrapy.http import Request
from jackpot.items import statAreaItems
from fuzzywuzzy import fuzz
import json
from datetime import datetime

def loadSPFile():
	try:
		with open('sportpesa.json') as data_file:
			data = json.load(data_file)
	except IOError:
		return []
	return data

def matchdates(data):
	dates = []
	for d in data:
		if d['date'] not in dates:
			dates.append(d['date'])
	return dates

def formatDates(dates, currentformat, expectedformat):
	formattedDates = []
	for date in dates:
		print date
		dateObj = datetime.strptime(date, currentformat).date()
		cleandate = dateObj.strftime(expectedformat)
		formattedDates.append(cleandate)
	return formattedDates

class StatareaSpider(scrapy.Spider):
	name = "statarea"
	allowed_domains = ["http://www.statarea.com/"]
	dates = matchdates(loadSPFile())

	def start_requests(self):
		urldates = formatDates(self.dates, '%d/%m/%y', '%Y-%m-%d')
		for urldate in urldates:
			yield Request('http://www.statarea.com/predictions/date/'+urldate+'/competition', self.parse)

	def parse(self, response):
		data = loadSPFile()
		items = statAreaItems()
		urlparts = response.url.split('/')
		date = []
		date.append(urlparts[-2])
		datef = formatDates(date, '%Y-%m-%d', '%d/%m/%y')

		for event in data:
			if (event['date'] == datef[0]):
				matches = response.css('.prdmatch')
				for matchinfo in matches:
					teamname = matchinfo.css('.prdhostteam a::text').extract()[0]
					if(fuzz.ratio(teamname.lower().strip(), event['homet'].lower().strip()) > 70 ):
						items['pos'] = event['pos']
						items['tips'] = matchinfo.css('.tipfield span::text').extract()
						print event['homet'] +" - "+event['awayt'] + " odds --> "+''.join(items['tips'])
						yield(items)
