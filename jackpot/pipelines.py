# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html


class JackpotPipeline(object):
	def process_item(self, item, spider):
		if item['date']:
			item['date'] = item['date'].replace('\n', '')
			item['date'] = item['date'].strip()
		return item
